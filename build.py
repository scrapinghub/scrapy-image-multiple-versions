#!/usr/bin/env python
import subprocess
from pathlib import Path
import os
import json

envs = Path.cwd() / 'envs'
os.makedirs(envs, exist_ok=True)
spiders = Path.cwd() / 'spiders'
names = []
for spider in spiders.glob('*'):
    if not spider.is_dir():
        continue
    name = spider.stem
    names.append(name)
    python = (spider / 'python.txt').read_text().splitlines()[0]
    env = envs / name
    subprocess.check_call([python, '-m', 'venv', env])
    subprocess.check_call([
        env / 'bin' / 'pip', 'install', '-r', spider / 'requirements.txt'])
    subprocess.check_call([
        env / 'bin' / 'python', 'setup.py', 'install'
    ], cwd=spiders, env={'INSTALL_SPIDER': name})
    subprocess.check_call([
        env / 'bin' / 'python', 'setup.py', 'clean', '--all',
    ], cwd=spiders, env={'INSTALL_SPIDER': name})

image_info = Path('/usr/local/bin/shub-image-info')
with image_info.open('w') as dest:
    dest.write('#!/bin/bash\n')
    dest.write('echo \'{}\'\n'.format(json.dumps(dict(
        project_type='scrapy',
        spiders=names,
    ))))
image_info.chmod(0o755)
