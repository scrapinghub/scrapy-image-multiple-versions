from setuptools import setup, find_packages
import os

name = os.environ['INSTALL_SPIDER']

setup(
    name=name,
    version='1.0',
    packages=[p for p in find_packages() if p.split('.')[0] == name],
    entry_points={'scrapy': [f'settings = {name}.settings']},
)
