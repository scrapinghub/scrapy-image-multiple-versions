FROM fkrull/multi-python
RUN apt-get update && apt-get install -y python3.6-dev python3.6-venv python3.7-venv python3.7-dev
COPY ./bin /usr/local/bin
COPY ./spiders /spiders
ADD build.py /
RUN python3.6 /build.py
ENV PATH=/usr/local/bin:$PATH
